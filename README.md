
# Files

taken from ftp.dlink.de/dsp/dsp-w245

# binwalk

Found on https://github.com/ReFirmLabs/binwalk/blob/master/INSTALL.md


## Another try

Successful !

https://github.com/ReFirmLabs/binwalk/wiki/Quick-Start-Guide

git clone repo
sudo python3 setup.py install

binwalk -Me DSPW245_A1_FW101b03.img

https://github.com/ReFirmLabs/binwalk

## Failed attempt

Different order than in the readme:

sudo apt install python3-pip
sudo pip3 install nose coverage


# Analysis

Interesting: 

repos/hacking-dsp-w245/files/_DSPW245_A1_FW101b03.img.extracted/_40.extracted/_22F000.extracted/cpio-root/etc_ro/web/cgi-bin

-rwxr-xr-x 1 mg mg   383 Jun 27 20:54 ExportSettings.sh*
-rw-r--r-- 1 mg mg  2045 Jun 27 20:54 History
-rwxr-xr-x 1 mg mg   694 Jun 27 20:54 history.sh*
-rwxr-xr-x 1 mg mg    89 Jun 27 20:54 reboot.sh*
-rwxr-xr-x 1 mg mg  6324 Jun 27 20:54 test.cgi*
-rwxr-xr-x 1 mg mg 10352 Jun 27 20:54 upload_bootloader.cgi*
-rwxr-xr-x 1 mg mg 14408 Jun 27 20:54 upload.cgi*
-rwxr-xr-x 1 mg mg  8740 Jun 27 20:54 upload_settings.cgi*

# After update

Firmware Version: 1.01-b03
Date: Sep 26, 2019
Checksum: 0x08A49B24
2.4GHz regulation domain: EU
1,2,3,4,5,6,7,8,9,10,11,12,13
2.4GHz SSID: DSP-W245-ACCF
LAN MAC: 74:DA:DA:5F:AC:CF
2.4GHz WLAN MAC: 74:DA:DA:5F:AC:CF



# Other links

somehow TP-Link seems to be related, at least function names seem to be familiar:
https://www.hackster.io/machinekoder/controlling-tp-link-hs100-110-smart-plugs-with-machinekit-726145
https://github.com/Garfonso/dlinkWebSocketClient/issues/1#issuecomment-650777544

