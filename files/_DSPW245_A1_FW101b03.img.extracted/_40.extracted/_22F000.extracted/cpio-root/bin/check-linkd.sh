#!/bin/sh
# Created by Joy 2016-07-01

need_update="0"
#new_ver=0.11
new_ver=$(cat /etc_ro/linkd | grep '#version=' | cut -d '=' -f 2)
echo new_ver=$new_ver
new_ver_major=$(echo $new_ver | cut -d '.' -f 1)
echo new_ver_major=$new_ver_major
new_ver_minor=$(echo $new_ver | cut -d '.' -f 2)
echo new_ver_minor=$new_ver_minor

cur_ver=$(cat /dch/linkd | grep '#version=' | cut -d '=' -f 2)
echo cur_ver=$cur_ver
cur_ver_major=$(echo $cur_ver | cut -d '.' -f 1)
echo cur_ver_major=$cur_ver_major
cur_ver_minor=$(echo $cur_ver | cut -d '.' -f 2)
echo cur_ver_minor=$cur_ver_minor

if  [ "$cur_ver_major" -lt "$new_ver_major" ]; then
	echo "cur_ver_major less than new_ver_major"
	need_update="1"
elif [ "$cur_ver_major" == "$new_ver_major" ]; then
	echo "cur_ver_major == new_ver_major"
	if  [ "$cur_ver_minor" -lt "$new_ver_minor" ]; then
		echo "cur_ver_minor less than new_ver_minor"
		need_update="1"
	fi
fi

echo need_update=$need_update

#if  [ "$cur_ver" != "$new_ver" ]; then
if  [ "$need_update" == "1" ]; then
#	printf "\E[0;35;40m"
	echo "== linkd need update !! ============"
	cp /etc_ro/linkd /dch/linkd
	cp /etc_ro/launcher_key /dch/launcher_key
#	printf "\E[0m"
fi

exit 0
