#!/bin/sh

echo -e "\033[41;37m get-ap-list called \033[0m"

# Added by Joy 2017-11-13
Channel=`nvram_get Channel`
echo "Channel=$Channel"

Ra0up=`nvram_get Ra0up`
echo "Ra0up=$Ra0up"

ApCliSsid=`nvram_get ApCliSsid`
echo "ApCliSsid=$ApCliSsid"

ApCliAuthMode=`nvram_get ApCliAuthMode`
echo "ApCliAuthMode=$ApCliAuthMode"

ApCliEncrypType=`nvram_get ApCliEncrypType`
echo "ApCliEncrypType=$ApCliEncrypType"

ApCliWPAPSK=`nvram_get ApCliWPAPSK`
echo "ApCliWPAPSK=$ApCliWPAPSK"

ApCliDefaultKeyID=`nvram_get 2860 ApCliDefaultKeyID`
echo "ApCliDefaultKeyID=$ApCliDefaultKeyID"

ApCliKey1Type=`nvram_get 2860 ApCliKey1Type`
echo "ApCliKey1Type=$ApCliKey1Type"

ApCliKey1Str=`nvram_get 2860 ApCliKey1Str`
echo "ApCliKey1Str=$ApCliKey1Str"


iwpriv ra0 set SiteSurvey=1
iwpriv ra0 get_site_survey

#userconf set ScheduleNumbers 0
#echo "reset ScheduleNumbers 0 ---------"

#if [ "$cronebl" = "1" ]; then
#fi

exit 0

# Last Modified: 2018-01-04 11:33
