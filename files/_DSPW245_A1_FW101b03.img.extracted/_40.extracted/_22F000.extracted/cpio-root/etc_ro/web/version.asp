<html>
<head>
<title>version.txt</title>
<link rel="stylesheet" href="/style/normal_ws.css" type="text/css">
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<!--META HTTP-EQUIV="refresh" CONTENT="3; URL=./status-user.asp"-->

<script language="JavaScript" type="text/javascript">
Butterlate.setTextDomain("admin");

function style_display_on()
{
	if (window.ActiveXObject)
	{ // IE
		return "block";
	}
	else if (window.XMLHttpRequest)
	{ // Mozilla, Safari,...
		return "table-row";
	}
}

function showOpMode()
{
	var opmode = 1* <% getCfgZero(1, "OperationMode"); %>;
	if (opmode == 0)
		document.write("Bridge Mode");
	else if (opmode == 1)
		document.write("Gateway Mode");
	else if (opmode == 2)
		document.write("Ethernet Converter Mode");
	else if (opmode == 3)
		document.write("AP Client Mode");
	else
		document.write("Unknown");
}

</script>
</head>

<body>
<table class="body"><tr><td>
<table width="95%" border="1" cellpadding="2" cellspacing="1">
<!-- ================= System Info ================= -->
  <tr>
    <td class="head" id="statusSDKVersion">Firmware Version:</td>
    <td>v1.00-b16</td>
  </tr>
  <tr>
    <td class="head">Date:</td>
    <td><% getSysBuildTime(); %></td>
  </tr>
  <tr>
    <td class="head">Checksum:</td>
    <td>0x14A48A15</td>
  </tr>
  <!--tr>
    <td class="head" id="statusOPMode">Operation Mode</td>
    <td><script type="text/javascript">showOpMode();</script></td>
  </tr-->
  <tr>
    <td class="head" id="basicSSID">2.4GHz regulation domain:</td>
    <td>EU  (1,2,3,4,5,6,7,8,9,10,11,12,13)</td>
  </tr>
  <tr>
    <td class="head" id="basicSSID">2.4GHz SSID:</td>
    <td><% getCfgToHTML(1, "SSID1"); %></td>
  </tr>
  <tr>
    <td class="head" id="statusLANMAC">2.4GHz WLAN MAC:</td>
    <td><% getLanMac(); %></td>
  </tr>
</table>

</td></tr></table>
</body>
</html>
