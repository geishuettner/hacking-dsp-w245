#!/bin/sh
killall -q crond
mkdir -p /var/spool/cron/crontabs
rm -f /var/spool/cron/crontabs/admin
cronebl="0"
crossday="0"

for j in 1 2 3 4 5
do
# Remove /mnt/user/ by Joy 2016-04-26
   GreenAPModules=`userconf get GreenAP${j}Modules`

    moduleID=${GreenAPModules%%,*}
    gpioRemain=${GreenAPModules#*,}
    while [ -n "$moduleID" ]
    do
#transform ModuleID to GPIO
        if [ "$moduleID" = "1" ]; then
	        gpio1="11"
        elif [ "$moduleID" = "3" ]; then
	        gpio1="17"
        elif [ "$moduleID" = "5" ]; then
	        gpio1="19"
        elif [ "$moduleID" = "7" ]; then
	        gpio1="18"
        else
	        gpio1=${moduleID}
        fi
# Remove /mnt/user/ by Joy 2016-04-26
        start=`userconf get GreenAP${j}Start1`
        end=`userconf get GreenAP${j}End1`
        week=`userconf get GreenAP${j}Week1`
        if [ "$week" = "" -o "$week" = "all" ]; then
	        week="*"
        elif [ "$week" = "7" ]; then
	        week="0"
        fi
        start_first=${start}
        end_first=${end}
        week_first=${week}
        i=2

        while [ -n "$start" ] #start is not an empty string 
        do 
            cronebl="1"
            if [ "$crossday" = "1" ]; then
                crossday="0"
            else
                echo "$start * * $week GpioForCrond 1 $gpio1" >> /var/spool/cron/crontabs/admin
            fi
#if 24:00 is read, modify it to 00:00 of next day
            if [ "$end" = "0 24" ]; then
                end="0 0"
	            week=`expr $week + 1`
                if [ "$week" = "8" ]; then
	                week="1"
                elif [ "$week" = "7" ]; then
	                week="0"
                fi
            fi
            start_str="GreenAP${j}Start$i"
            end_str="GreenAP${j}End$i"
            week_str="GreenAP${j}Week$i"
# Remove /mnt/user/ by Joy 2016-04-26
            start_next=`userconf get $start_str`
            end_next=`userconf get $end_str`
            week_next=`userconf get $week_str`
            if [ "$week_next" = "" -o "$week_next" = "all" ]; then
	            week_next="*"
            fi
            if [ "$week_next" = "7" ]; then
	            week_next="0"
            fi
            if [ -n "$start_next" ]; then #start is not an empty string 
                if [ "$start_next" = "0 0" ] && [ "$end" = "0 0" ] && [ "$week_next" = "$week" ]; then
                    crossday="1"
                else
                    echo "$end * * $week GpioForCrond 0 $gpio1" >> /var/spool/cron/crontabs/admin
                fi
            elif [ "$start_first" = "0 0" ] && [ "$end" = "0 0" ] && [ "$week_first" = "$week" ]; then
#this variable is meaningless, just to avoid syntax error
                wholeweek="1"
            else
                echo "$end * * $week GpioForCrond 0 $gpio1" >> /var/spool/cron/crontabs/admin
            fi
            start=${start_next}
            end=${end_next}
            week=${week_next}
            i=`expr $i + 1` 
        done

        if [ "$moduleID" = "$gpioRemain" ] ; then
            break
        fi

        moduleID=${gpioRemain%%,*}
        gpioRemain=${gpioRemain#*,}
    done
done

for j in 1 2 3 4 5
do
# Remove /mnt/user/ by Joy 2016-04-26
    GreenAPModules=`userconf get GreenAP_R${j}Modules`

    moduleID=${GreenAPModules%%,*}
    gpioRemain=${GreenAPModules#*,}
    while [ -n "$moduleID" ]
    do
#transform ModuleID to GPIO
        if [ "$moduleID" = "1" ]; then
	        gpio1="11"
        elif [ "$moduleID" = "3" ]; then
	        gpio1="17"
        elif [ "$moduleID" = "5" ]; then
	        gpio1="19"
        elif [ "$moduleID" = "7" ]; then
	        gpio1="18"
        else
	        gpio1=${moduleID}
        fi
# Remove /mnt/user/ by Joy 2016-04-26
        start=`userconf get GreenAP_R${j}Start1`
        end=`userconf get GreenAP_R${j}End1`
        week=`userconf get GreenAP_R${j}Week`
        if [ "$week" = "" -o "$week" = "all" ]; then
	        week="*"
        fi
        i=2

        while [ -n "$start" ] #start is not an empty string 
        do 
            cronebl="1"
            echo "$start * * $week GpioForCrond 1 $gpio1" >> /var/spool/cron/crontabs/admin
            echo "$end * * $week GpioForCrond 0 $gpio1" >> /var/spool/cron/crontabs/admin
            start_str="GreenAP_R${j}Start$i"
            end_str="GreenAP_R${j}End$i"
            week_str="GreenAP_R${j}Week"
# Remove /mnt/user/ by Joy 2016-04-26
            start=`userconf get $start_str`
            end=`userconf get $end_str`
            week=`userconf get $week_str`
            if [ "$week" = "" -o "$week" = "all" ]; then
	            week="*"
            fi
            i=`expr $i + 1` 
        done

        if [ "$moduleID" = "$gpioRemain" ] ; then
            break
        fi

        moduleID=${gpioRemain%%,*}
        gpioRemain=${gpioRemain#*,}
    done
done

if [ "$cronebl" = "1" ]; then
    crond
fi

exit 0
