#!/bin/sh

# Added by Joy 2018-03-12
opmode=`nvram_get OperationMode`
factory_mode=`userconf get FactoryMode`
#mdns_conf_file="/mnt/user/mdns/mdns.conf"
#mac_last=`mdb get mac_addr | cut -c 9-`
#md_ver=`cat /mydlink/version`

# Added by Joy 2018-03-06
iptables -F
# Added by Joy 2018-03-30
# for fw_upgrade wget
iptables -A INPUT -p tcp --sport 80 -j ACCEPT

# Added by Joy 2018-03-12
# for SSL
iptables -A INPUT -p tcp --sport 443 -j ACCEPT

# for telnetd
#if [ "$factory_mode" = "1" ]; then 
iptables -A INPUT -p tcp --dport 23 -j ACCEPT
#else
#	factory_mode=`nvram_get FactoryMode`
#	if [ "$factory_mode" = "1" ]; then 
#		iptables -A INPUT -p tcp --dport 23 -j ACCEPT
#	fi
#fi

# for QA http://[ip]/version.txt
iptables -A INPUT -p tcp --dport 80 -j ACCEPT
# for App to Agent
iptables -A INPUT -p tcp --dport 8080 -j ACCEPT
iptables -A INPUT -p tcp --dport 8081 -j ACCEPT
# for DNS
#iptables -A INPUT -p udp --dport 53 -j ACCEPT
if [ "$opmode" != "2" ]; then
	# for DHCP server listen (broadcast)
	iptables -A INPUT -p udp --dport 67 -j ACCEPT
fi
# Modified by Joy 2018-08-09
# for fw to Agent
iptables -A INPUT -p udp -d 127.0.0.1 --dport 3012 -j ACCEPT

# for zeroconf/mDNSResponder listen
iptables -A INPUT -p udp --dport 5353 -j ACCEPT
# for DNS response
iptables -A INPUT -p udp --sport 53 -j ACCEPT
# for NTP response
iptables -A INPUT -p udp --sport 123 -j ACCEPT
# other
iptables -A INPUT -p udp -j DROP
iptables -A INPUT -p tcp -j DROP

#debug
echo "opmode=$opmode"
#echo "serv=$srv"
#echo "sync=$sync"
#echo "tz=$tz"

#echo "mac_last=$mac_last" > $mdns_conf_file
#echo "md_ver=$md_ver" >> $mdns_conf_file

# Modified by Joy 2016-12-22
#if [ -e $mdns_conf_file ]; then
#	cat $mdns_conf_file
#fi


# Last Modified by Joy 2018-08-09 09:41
